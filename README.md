# FileSqueezer
#### Simple File Compressor & Extractor

[This console based software](http://minhaskamal.github.io/FileSqueezer) implements Huffman algorithm for file compression and extraction. It works on all types of files, but is only effective with text files.

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>FileSqueezer is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
